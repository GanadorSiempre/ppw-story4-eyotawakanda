"""ppw_story4_eyotawakanda URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from story4_eyotawakanda import views

urlpatterns = [
    path('admin/', admin.site.urls),
    # path('', include('')),
    path('story6/', include('story6_eyotawakanda.urls')),
    path('story4/', include('story4_eyotawakanda.urls')),
    path('story7/', include('story7_eyotawakanda.urls')),
    path('story8/', include('story8_eyotawakanda.urls')),
    path('', include('story9_eyotawakanda.urls')),
    path('accounts/', include('django.contrib.auth.urls')),
]
