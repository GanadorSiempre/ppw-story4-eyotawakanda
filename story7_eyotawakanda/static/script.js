$('.title').click(function(e){
    console.log('works')

    let titleAccor = $(this).parent().parent().next()
    if(titleAccor.hasClass("extend")){
        titleAccor.removeClass("extend")
        titleAccor.slideToggle(1000)
    }
    else{
        titleAccor.toggleClass("extend")
        titleAccor.slideUp(1000)
    }
})

$('.move-up').click(function(e){
    console.log('up')

    let moveup = $(this).parent().parent().parent()
    moveup.prev().insertAfter(moveup)
})

$('.move-down').click(function(e){
    console.log('down')

    let movedown = $(this).parent().parent().parent()
    movedown.next().insertBefore(movedown)
})