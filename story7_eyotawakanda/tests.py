from django.test import TestCase, Client
from .models import Orang, Aktivitas
from django.urls import reverse

class TestCaseStory7(TestCase):
    def test_root_url(self):
        respon = Client().get('/', follow=True)
        self.assertEqual(respon.status_code, 200)

    def test_views_check(self):
        respon = Client().get('/', follow=True)
        self.assertIn('Mimpi', str(respon.content))

# Create your tests here.
