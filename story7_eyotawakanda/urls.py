from django.urls import path
from . import views

app_name = 'story7_eyotawakanda'

urlpatterns = [
    path('', views.index, name="index"),
]
