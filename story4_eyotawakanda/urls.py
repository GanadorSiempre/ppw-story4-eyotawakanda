from django.urls import path
from . import views

app_name = 'story4_eyotawakanda'

urlpatterns = [
    path('', views.home, name='home'),
    path('contact/', views.contact, name='contact'),
    path('profile/', views.profile, name='profile'),
]

