from django.shortcuts import render, redirect
from django.http import request
from .forms import FormOrang
from .models import Orang, Aktivitas

def index(request):
    orang = Orang.objects.all()
    aktivitas = Aktivitas.objects.all()
    context = {
        'orang':orang,
        'aktivitas':aktivitas,
        'judul':'Aktivitas'
    }
    return render(request, 'index6.html', context)

def tambah(request):
    form = FormOrang(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('story6_eyotawakanda:index')
    context = {
        'judul':'tambah',
        'form':form
    }
    return render(request, "tambah.html", context)
# Create your views here.
