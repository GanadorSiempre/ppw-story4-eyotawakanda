from django.db import models

class Aktivitas(models.Model):
    nama_aktivitas = models.CharField(max_length=100)
    def __str__(self):
        return self.nama_aktivitas

class Orang(models.Model):
    nama_orang = models.CharField(max_length=100)
    aktivitas = models.ForeignKey('Aktivitas', on_delete = models.CASCADE)
    def __str__(self):
        return self.nama_orang
# Create your models here.