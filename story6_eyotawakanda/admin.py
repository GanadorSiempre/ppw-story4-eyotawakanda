from django.contrib import admin
from .models import Orang, Aktivitas

admin.site.register(Orang)
admin.site.register(Aktivitas)
# Register your models here.
