from django.forms import ModelForm
from django.db.models import fields
from .models import Orang

class FormOrang(ModelForm):
    class Meta:
        model = Orang
        fields = ['nama_orang', 'aktivitas']