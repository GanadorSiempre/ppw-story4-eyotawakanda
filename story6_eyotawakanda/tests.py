from django.test import TestCase, Client
from .models import Orang, Aktivitas
from django.urls import reverse

class TestCaseStory(TestCase):
    def init():
        aktivitas_object = Aktivitas.objects.create(
            nama_aktivitas = "Aktivitas 1"
        )
        orang_object = Orang.objects.create(
            nama_orang = "Orang 1",
            aktivitas = aktivitas_object
        )

    def test_root_url_check(self):
        respon = Client().get('/', follow=True)
        self.assertEqual(respon.status_code, 200)

    def test_tambah_url_check(self):
        respon = Client().get('/tambah', follow=True)
        self.assertEqual(respon.status_code, 200)

    def test_views_check(self):
        aktivitas_objek = Aktivitas.objects.create(nama_aktivitas='Aktivitas 1')

        respon = Client().post(reverse('story6_eyotawakanda:tambah'), {
            'nama_orang':'Orang 1',
            'aktivitas': 1,
        }, 
        follow=True
        )
        self.assertEqual(respon.status_code, 200)

    def test_model_check(self):
        aktivitas_objek = Aktivitas.objects.create(nama_aktivitas="Aktivitas 1")
        orang_objek = Orang.objects.create(nama_orang='Orang 1', aktivitas=aktivitas_objek)
        
        aktivitas_objek = Aktivitas.objects.get(nama_aktivitas="Aktivitas 1")
        self.assertEquals(aktivitas_objek.nama_aktivitas, "Aktivitas 1")
        
        orang_objek = Orang.objects.get(nama_orang='Orang 1')
        self.assertEquals(orang_objek.nama_orang, 'Orang 1')
        self.assertEquals(orang_objek.aktivitas, aktivitas_objek)
# Create your tests here.