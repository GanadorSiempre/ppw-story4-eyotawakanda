from django.urls import path
from . import views

app_name = 'story9_eyotawakanda'

urlpatterns = [
    path('', views.index, name="index"),
]
