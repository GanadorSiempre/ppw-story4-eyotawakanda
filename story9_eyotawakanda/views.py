from django.http import response
from django.shortcuts import render, redirect
from .forms import CreateUserForm
from django.contrib.auth import authenticate, login
from django.contrib.auth.forms import UserCreationForm

def index(request):
    form = UserCreationForm()
    message = "Create your account"
    if request.method == "POST":
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            user = authenticate(username=form.cleaned_data.get("username"),
            password=form.cleaned_data.get("password1"))
            login(request, user)
    context = {'page_title': 'Login', 'form' : form}
    return render(request, 'index9.html', context)


# Create your views here.
