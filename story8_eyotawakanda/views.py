from django.http import JsonResponse
from django.http import response
from django.shortcuts import render
import urllib3
import json


def index(request):
    return render(request, 'index8.html')

def request(request):
        searched = urllib3.PoolManager()
        response = searched.request('GET', "https://www.googleapis.com/books/v1/volumes?" +request.GET.urlencode())
        context = json.loads(response.data.decode('utf-8'))

        return JsonResponse(context)

# Create your views here.
