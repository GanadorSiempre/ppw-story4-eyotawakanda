from django.urls import path
from . import views

app_name = 'story8_eyotawakanda'

urlpatterns = [
    path('', views.index, name="index"),
    path('request/', views.request, name="request")
]
