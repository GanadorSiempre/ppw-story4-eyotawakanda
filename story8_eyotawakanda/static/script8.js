$('#search-form').submit(function (e) {
    e.preventDefault()
    let search = $('#search-input').val()
    console.log(window.location.origin + "/request")
    $.ajax({
        url: window.location.origin + "/request",
        contentType: "application/json",
        data: {
            "q": search,
        },
        dataType: 'json',
        success: function (result) {
            let parent = $("#result");
            let inner = "";
            let json = result.items;
            for (let i = 0; i < json.length; i++) {
                let data = json[i].volumeInfo
                inner += '<div class="search-output">'
                inner += '<a target="_blank" href="' + data.infoLink + '">'
                inner += '<img src="' + data.imageLinks===undefined ? 'No Image' : data.imageLinks.thumbnail + '"/>'
                inner += "<h6>" + data.title + "</h6>"
                inner += "<p>" + data.authors + "</p>"
                inner += "</a></div>"
            }
            parent.html(inner)
        }

    });

}
)

